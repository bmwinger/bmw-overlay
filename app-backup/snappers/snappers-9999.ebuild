# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python2_7 )

inherit git-r3 distutils-r1

DESCRIPTION="A set of supplemental subcommands for snapper."
HOMEPAGE="https://github.com/ddworken/snapperS"
EGIT_REPO_URI="https://github.com/ddworken/snapperS.git"

LICENSE="GPL-2"
SLOT="0"

REQUIRED_USE="${PYTHON_REQUIRED_USE}"
PYTHON_REQ_USE="ssl,threads"

DEPEND="${PYTHON_DEPS}
dev-python/configargparse[${PYTHON_USEDEP}]
dev-python/tabulate[${PYTHON_USEDEP}]"
RDEPEND="${DEPEND}"
