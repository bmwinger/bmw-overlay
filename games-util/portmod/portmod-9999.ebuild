# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

PYTHON_COMPAT=( python3_6 python3_7 )

inherit git-r3 distutils-r1

DESCRIPTION="A CLI tool to manage mods for OpenMW"
HOMEPAGE="https://gitlab.com/portmod/portmod"
EGIT_REPO_URI="https://gitlab.com/portmod/portmod.git"

LICENSE="GPL-3"
SLOT="0"

REQUIRED_USE="${PYTHON_REQUIRED_USE}"

PYTHON_REQ_USE="ssl,threads"

DEPEND="${PYTHON_DEPS}
	app-arch/patool
	dev-python/colorama[${PYTHON_USEDEP}]
	dev-python/appdirs[${PYTHON_USEDEP}]
	dev-python/git-python[${PYTHON_USEDEP}]
	dev-python/pyyaml[${PYTHON_USEDEP}]
	dev-python/progressbar2[${PYTHON_USEDEP}]
	dev-python/black[${PYTHON_USEDEP}]
	dev-python/pytest[${PYTHON_USEDEP}]
	dev-python/RestrictedPython[${PYTHON_USEDEP}]
"
RDEPEND="${DEPEND}"
