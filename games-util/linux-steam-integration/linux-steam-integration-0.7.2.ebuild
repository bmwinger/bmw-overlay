# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

inherit meson

DESCRIPTION="Helper for enabling better Steam integration on Linux"
HOMEPAGE="https://github.com/solus-project/linux-steam-integration"
SRC_URI="https://github.com/solus-project/${PN}/releases/download/v${PV}/${P}.tar.xz"

LICENSE="LGPL-2.1"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE="frontend"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

src_configure() {
	local emesonargs=(
			-Dwith-shim=co-exist
			-Dwith-steam-binary=/usr/bin/steam
			-Dwith-frontend=$(usex frontend true false)
	)
	meson_src_configure
}
