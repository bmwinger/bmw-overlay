# Copyright 2019 Gentoo Authors
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="A program to roll dice in a simple and intuitive way"
HOMEPAGE="https://github.com/matteocorti/roll"
SRC_URI="https://github.com/matteocorti/roll/releases/download/v${PV}/roll-${PV}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

src_configure() {
	econf --with-posix-regex
}

src_install() {
	emake DESTDIR="${D}" install

	dodoc NEWS README
}
