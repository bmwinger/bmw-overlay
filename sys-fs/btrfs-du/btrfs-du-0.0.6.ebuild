# Copyright 1999-2018 Gentoo Foundation
# Distributed under the terms of the GNU General Public License v2

EAPI=7

DESCRIPTION="A tool for easily printing BTRFS subvolume/snapshot disk usage"
HOMEPAGE="https://github.com/nachoparker/btrfs-du"
SRC_URI="https://github.com/nachoparker/${PN}/archive/v${PV}.tar.gz"

LICENSE="GPL-2"
SLOT="0"
KEYWORDS="~amd64 ~x86"
IUSE=""

DEPEND="sys-fs/btrfs-progs"
RDEPEND="${DEPEND}"
BDEPEND=""

src_install() {
	dobin btrfs-du
	dodoc README.md
}
